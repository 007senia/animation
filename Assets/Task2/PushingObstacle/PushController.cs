using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushController : MonoBehaviour
{
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void PushChange()
    {
        int randomInt = Random.Range(0, 3);

        anim.SetInteger("IntPush", randomInt);
    }
}
