using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SpinController : MonoBehaviour
{
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    void RotationChange()
    {
        int randomInt = Random.Range(0, 3);

        anim.SetInteger("IntSpin", randomInt);
    }
}
